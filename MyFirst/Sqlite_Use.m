//
//  Sqlite_Use.m
//  MyFirst
//
//  Created by 靳杰 on 14-4-24.
//  Copyright (c) 2014年 靳杰. All rights reserved.
//

#import "Sqlite_Use.h"
#import "/usr/include/sqlite3.h"

@interface Sqlite_Use ()
- (NSString *) dataFilePath;// 查找文件路径
- (void) sqliteOpen; // 打开数据库
- (BOOL) sqliteCheck;
@end

@implementation Sqlite_Use
@synthesize name;
@synthesize birth;
@synthesize idention;

-(id)initWithName:(NSString *)inName birth:(NSString *)inBirth idention:(NSString *)inIdention{
    if (self = [self init]) {
        [self setName:inName];
        [self setBirth:inBirth];
        [self setIdention:inIdention];
    }
    return self;
}

-(BOOL) sqliteCheck{
    sqlite3 *database;
    if (sqlite3_open([[self dataFilePath] UTF8String], &database)!=SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"open database faid!");
        NSLog(@"数据库查找失败！");
        return false;
    }
    else{
        NSString *query=@"select *from personinfo";
        sqlite3_stmt *stmt;
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &stmt, nil) == SQLITE_OK) {
            while (sqlite3_step(stmt)==SQLITE_ROW) {
                char *nametemp = (char *)sqlite3_column_text(stmt, 1);
                NSString *nameString = [[NSString alloc] initWithUTF8String:nametemp];
                self.name = nameString;
                
                int birthtemp = sqlite3_column_int(stmt, 2);
                self.birth = [NSString stringWithFormat:@"%d",birthtemp];
                
                char *identiontemp = (char *)sqlite3_column_text(stmt, 3);
                NSString *identionString = [[NSString alloc] initWithUTF8String:identiontemp];
                self.idention = identionString;
                NSLog(@"%@,%@,%@\n",self.name,self.birth,self.idention);
            }
            
            sqlite3_finalize(stmt);
        }
        //用完了一定记得关闭，释放内存
        sqlite3_close(database);
        return YES;
    }
}

- (void) sqliteOpen{
    sqlite3 *database;
    if (sqlite3_open([[self dataFilePath] UTF8String], &database)!=SQLITE_OK) {
        sqlite3_close(database);
        NSAssert(0, @"open database faid!");
        NSLog(@"数据库创建失败！");
    }
    else{
        NSString *ceateSQL = @"CREATE TABLE IF NOT EXISTS personinfo(ID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT,birth TEXT,idention TEXT)";
    
        char *ERROR;
    
        if (sqlite3_exec(database, [ceateSQL UTF8String], NULL, NULL, &ERROR)!=SQLITE_OK){
            sqlite3_close(database);
            NSAssert(0, @"ceate table faild!");
            NSLog(@"表创建失败");
        }
        else {
            char *saveSQL = "INSERT OR REPLACE INTO personinfo(name,birth,idention)""VALUES(?,?,?);";
            
            char *errorMsg = NULL;
            sqlite3_stmt *stmt;
            
            if (sqlite3_prepare_v2(database, saveSQL, -1, &stmt, nil) == SQLITE_OK) {
                
                sqlite3_bind_text(stmt, 1, [self.name UTF8String], -1, NULL);
                sqlite3_bind_int(stmt, 2, [self.birth intValue]);
                sqlite3_bind_text(stmt, 3, [self.idention UTF8String], -1, NULL);
            }
            if (sqlite3_step(stmt) != SQLITE_DONE) {
                NSLog(@"数据更新失败");
                NSAssert(0, @"error updating :%s",errorMsg);
            }
            sqlite3_finalize(stmt);
            sqlite3_close(database);
        }
    }
    
}

-(NSString *) dataFilePath{
    NSArray *path =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *document = [path objectAtIndex:0];
    return [document stringByAppendingPathComponent:@"test.sqlite"];
}

@end
