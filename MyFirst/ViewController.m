//
//  ViewController.m
//  MyFirst
//
//  Created by 靳杰 on 13-12-30.
//  Copyright (c) 2013年 靳杰. All rights reserved.
//

#import "ViewController.h"
#import "Sqlite_Use.h"

@interface ViewController ()
- (IBAction)getResult:(id)sender;
- (UIImageView *)rotate360DegreeWithImageView:(UIImageView *)imageView;
- (IBAction)editFinished:(id)sender;
- (IBAction)backgroundTap:(id)sender;
@end


@implementation ViewController
@synthesize name;
@synthesize birth;
@synthesize idention;
@synthesize result;
@synthesize button;
@synthesize img;
// 点击其他位置消失键盘
- (IBAction)backgroundTap:(id)sender{
    [name resignFirstResponder];
    [birth resignFirstResponder];
    [idention resignFirstResponder];
}
// 点击键盘return消失键盘
- (IBAction)editFinished:(id)sender
{
    [sender resignFirstResponder];
}
// 点击按钮事件
- (IBAction)getResult:(id)sender
{
    NSString *s = [NSString stringWithFormat:@"姓名：%@,生日：%@,证件号：%@.", name.text, birth.text,idention.text];
    [result setText:s];
    [self rotate360DegreeWithImageView:self.img];
    self.datasql = [[Sqlite_Use alloc] initWithName:name.text birth:birth.text idention:idention.text];
    [[self datasql] sqliteOpen];
    if ([[self datasql] sqliteCheck]) {
        [result setText:[NSString stringWithFormat:@"来自数据库查询：\n姓名：%@,生日：%@,证件号：%@.",[[self datasql] name],[[self datasql] birth],[[self datasql] idention]]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// 旋转图片
- (UIImageView *)rotate360DegreeWithImageView:(UIImageView *)imageView{
    CABasicAnimation *animation = [ CABasicAnimation
                                   animationWithKeyPath: @"transform" ];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [ NSValue valueWithCATransform3D:
                         
                         CATransform3DMakeRotation(M_PI, 0.0, 0.0, 1.0) ];
    animation.duration = 0.5;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = 1000;
    
    //在图片边缘添加一个像素的透明区域，去图片锯齿
    CGRect imageRrect = CGRectMake(0, 0,imageView.frame.size.width, imageView.frame.size.height);
    UIGraphicsBeginImageContext(imageRrect.size);
    [imageView.image drawInRect:CGRectMake(1,1,imageView.frame.size.width-2,imageView.frame.size.height-2)];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [imageView.layer addAnimation:animation forKey:nil];
    return imageView;
}

//animation.repeatCount = 1000;
//这个你要想一直旋转，设置一个无穷大就得了
//
//停止的话直接这样就停止了
//[self.view.layer removeAllAnimates];

@end