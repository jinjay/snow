//
//  AppDelegate.h
//  MyFirst
//
//  Created by 靳杰 on 13-12-30.
//  Copyright (c) 2013年 靳杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
