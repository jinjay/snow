//
//  Sqlite_Use.h
//  MyFirst
//
//  Created by 靳杰 on 14-4-24.
//  Copyright (c) 2014年 靳杰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sqlite_Use : NSObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *birth;
@property (nonatomic, retain) NSString *idention;
- (id)initWithName:(NSString *)inName birth:(NSString*)inBirth idention:(NSString*)inIdention;
- (void) sqliteOpen;
- (BOOL) sqliteCheck;
@end
