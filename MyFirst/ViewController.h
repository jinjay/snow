//
//  ViewController.h
//  MyFirst
//
//  Created by 靳杰 on 13-12-30.
//  Copyright (c) 2013年 靳杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sqlite_Use.h"

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, retain) IBOutlet UITextField *name;
@property (nonatomic, retain) IBOutlet UITextField *birth;
@property (nonatomic, retain) IBOutlet UITextField *idention;
@property (nonatomic, retain) IBOutlet UITextView *result;
@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, retain) IBOutlet UIImageView *img;
@property (nonatomic, retain) Sqlite_Use *datasql;
@end

