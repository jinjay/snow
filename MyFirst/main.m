//
//  main.m
//  MyFirst
//
//  Created by 靳杰 on 13-12-30.
//  Copyright (c) 2013年 靳杰. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
